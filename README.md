# README #

This README for news-app Repo.

### how to start this project ###

* run npm install on cmd in the path of package.json file.
* after downlaoding packages is done. run npm start on cmd.

### Notes ###

* the country select (dropdown) will appear only when navigate to headlines page as the "Everything" endpoint don't support the counrty field as parameter.

### Technology set ###

* JavaScript (ECMAScript).
* Reactjs.
* React-Router.
* Redux.
* Middlewars (Redux).

### Developed By ###

 Magdy Hussien