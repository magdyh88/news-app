import * as  React from 'react';

class FilterSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '', selectedSource: '0', selectedCountry: 'us', fromDate: '', toDate: ''
        };

        this.keyword = React.createRef();
        this.source = React.createRef();
        this.country = React.createRef();
        this.fromDate = React.createRef();
        this.toDate = React.createRef();

        this.handleChange = this.handleChange.bind(this);
        this.handleSearchbtn = this.handleSearchbtn.bind(this);
    }


    handleSearchbtn() {
        let { Api } = this.props;
        let { keyword } = this.state;
        if (window.location.pathname === '/headlines') {
            Api.getHeadlines(keyword, this.source.current.value, this.country.current.value, this.fromDate.current.value, this.toDate.current.value);
        }
        else {
            Api.getAllNews(keyword, this.source.current.value, this.fromDate.current.value, this.toDate.current.value);
        }
    }

    handleChange(event) {
        let { Api } = this.props;
        this.setState({
            [event.target.name]: event.target.value
        }, console.log(this.source));

        if (event.target.name !== "keyword") {
            if (window.location.pathname === '/headlines') {
                Api.getHeadlines(this.keyword.current.value, this.source.current.value, this.country.current.value, this.fromDate.current.value, this.toDate.current.value);
            }
            else {
                Api.getAllNews(this.keyword.current.value, this.source.current.value, this.fromDate.current.value, this.toDate.current.value);
            }
        }
    }

    render() {
        const { keyword, selectedSource, selectedCountry, fromDate, toDate } = this.state;
        const { sources, countries } = this.props;
        return (<>
            <div className="input-group m-1">
                <input ref={this.keyword} type="text" className="form-control" name={'keyword'} value={keyword} onChange={this.handleChange} placeholder="Search this blog" />
                <div className="input-group-append">
                    <button className="btn btn-secondary" type="button" onClick={this.handleSearchbtn}>
                        <i className="fa fa-search"></i>
                    </button>
                </div>
            </div>

            <div className="input-group m-1">
                <select ref={this.source} name={'selectedSource'} value={selectedSource} onChange={this.handleChange} className="form-control m-2 col-3" >
                    <option disabled value={'0'}>Select Source</option>
                    {
                        sources.map(source => {
                            return <option key={source.id} value={source.id}>{source.name}</option>
                        })
                    }
                </select>
                {
                    window.location.pathname === '/headlines' &&
                    <select ref={this.country} name={'selectedCountry'} value={selectedCountry} onChange={this.handleChange} className="form-control m-2 col-3" >
                        <option value={'0'} disabled>Select Country</option>
                        {
                            countries.map(country => {
                                return <option key={country.id} value={country.id}>{country.name}</option>
                            })
                        }
                    </select>
                }

                <input ref={this.fromDate} type={'date'} name={'fromDate'} value={fromDate} className={'form-control m-2'} onChange={this.handleChange} />

                <input ref={this.toDate} type={'date'} name={'toDate'} value={toDate} className={'form-control m-2'} onChange={this.handleChange} />
            </div>
        </>)
    }
}

export default FilterSection;