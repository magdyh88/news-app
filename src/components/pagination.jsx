import React from 'react';

const Pagination = (props) => {
    const handlePaginationClick =(event)=>
    {
        event.preventDefault();
        props.actions.updatePageNum(Number(event.target.innerHTML), props.pageSize, props.count);
    }

    const numberOfpages = props.listofIndex.length > 0 ? props.listofIndex.map(page => <li key={page} className="page-link" onClick={handlePaginationClick}>{page}</li>)
                            : <div></div>
    return <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-center">
            {numberOfpages}
        </ul>
    </nav>
}


export default Pagination;