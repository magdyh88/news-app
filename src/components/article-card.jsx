import React from 'react';

const ArticleCard = (props) => {
    const handleOpenModal = (event) => {
        event.preventDefault();
        props.openModal(props);
    }
    return (<div className="card border-light text-left p-3 m-3">
        <div className="row no-gutters position-relative">
            <div className="col-md-4">
                <img src={props.urlToImage} className="card-img" alt="..." />
            </div>
            <div className="col-md-8">
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    <p className="card-text text-left">{props.description}</p>
                </div>
            </div>
            <a href='/' className={'stretched-link'} alt="..." onClick={handleOpenModal}>{}</a>
        </div>

    </div>
    )
}

export default ArticleCard;