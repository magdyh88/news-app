import * as  React from 'react';
import { Link } from 'react-router-dom';

const QuickViewModel = (props) => {
    const showHideClassName = props.isOpen ? "modal display-block" : "modal display-none";

    return (
        <div className={showHideClassName}>
            <div className="card text-left p-3 m-3 modal-main">
                <div className="float-right">
                    <button type="button" className="close" data-dismiss="modal" onClick={props.handleCloseModal} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="row no-gutters">
                    <div className="col-md-4">
                        <img src={props.urlToImage} className="card-img" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">{props.title}</h5>
                            <p className="card-text"><small className="text-muted">Source: {props.source.name}</small></p>
                            <p className="card-text"><small className="text-muted">Author: {props.author}</small></p>
                        </div>
                    </div>
                </div>
                <div className="row no-gutters">
                    <p className="card-text">{props.description}</p>
                </div>
                <div className="row no-gutters">
                    <Link className="nav-link" to={{ pathname: '/artical-details/' + props.title, articale: props }}>View Details</Link>
                </div>
            </div>
        </div>
    )
}

export default QuickViewModel;