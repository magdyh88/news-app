import store from '../store';
import { APIkey, HttpMethod } from './constants'
import newsActions from '../actions/newsActions';
import { ActionType } from '../actions/constants';

const homeBaseUrl = 'https://newsapi.org/v2/everything?';
const headlineBaseUrl = 'https://newsapi.org/v2/top-headlines?';
const sourcesBaseUrl = 'https://newsapi.org/v2/sources?'

const data = [
    { id: 'us', name: 'United States' },
    { id: 'eg', name: 'Egypt' },
    { id: 'de', name: 'Germany' },
    { id: 'ru', name: 'Russia' }
]

const ClientApi = {
    getAllNews: (keyword, source, fromDate, toDate) => {
        let query = (keyword && keyword !== '') ? 'q=' + keyword + '&' : 'q=bitcoin&';
        query += (source && source !== '0') ? 'sources=' + source + '&' : '';
        query += (fromDate && fromDate !== '') ? 'from=' + fromDate + '&' : '';
        query += (toDate && toDate !== '') ? 'to=' + toDate + '&' : '';

        return {
            type: ActionType.GET_ALL_NEWS, value: setTimeout(
                fetch(homeBaseUrl + query + 'apiKey=' + APIkey, {
                    method: HttpMethod.GET,
                }).then((response) => response.json()
                ).then((data) => {
                    store.dispatch(newsActions.getAllNews(data.articles));
                }).catch((err) => {
                    return {
                        type: "ERROR",
                        err
                    }
                }), 1500)
        }
    },

    getHeadlines: (keyword, source, country = 'us', fromDate, toDate) => {
        let query = (keyword && keyword !== '') ? 'q=' + keyword + '&' : '';
        query += (source && source !== '0') ? 'sources=' + source + '&' : '';
        query += (country && country !== '0') ? 'country=' + country + '&' : '';
        query += (fromDate && fromDate !== '') ? 'from=' + fromDate + '&' : '';
        query += (toDate && toDate !== '') ? 'to=' + toDate + '&' : '';

        return {
            type: ActionType.GET_ALL_HEADLINES, value: setTimeout(
                fetch(headlineBaseUrl + query + 'apiKey=' + APIkey, {
                    method: HttpMethod.GET,
                }).then((response) => response.json()
                ).then((data) => {
                    store.dispatch(newsActions.getAllHeadlines(data.articles));
                }).catch((err) => {
                    return {
                        type: "ERROR",
                        err
                    }
                }), 1500)
        }
    },
    getAllSources: () => {
        return {
            type: ActionType.GET_ALL_Sources, value: setTimeout(
                fetch(sourcesBaseUrl + '&apiKey=' + APIkey, {
                    method: HttpMethod.GET,
                }).then((response) => response.json()
                ).then((data) => {
                    store.dispatch(newsActions.getSources(data.sources));
                }).catch((err) => {
                    return {
                        type: "ERROR",
                        err
                    }
                }), 1500)
        }
    },
    getCountries: () => {
        return {
            type: ActionType.GET_COUNTRIES, value: setTimeout(
                store.dispatch(newsActions.getCountries(data)), 1500)
        }
    }
}
export default ClientApi;