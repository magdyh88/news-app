import { ActionType } from '../actions/constants';

const newsReducer = (InitiState = { newsArr: [], sources: [], countries: [], pageSize: 5, pageIndex: 1, listofIndex: [] }, action) => {
    switch (action.type) {
        case ActionType.GET_ALL_NEWS:
            return {
                ...InitiState,
                newsArr: action.news
            }
        case ActionType.GET_ALL_HEADLINES:
            return {
                ...InitiState,
                newsArr: action.headlines
            }
        case ActionType.GET_ALL_Sources:
            return {
                ...InitiState,
                sources: action.sources
            }
        case ActionType.GET_COUNTRIES:
            return {
                ...InitiState,
                countries: action.countries
            }
        case ActionType.UPDATE_PAGE_NUM:
            let numberofIndex = 1;
            let listofIndex = [];
            numberofIndex = action.count / action.pageSize;

            for (let index = 1; index <= Math.ceil(numberofIndex); index++) {
                if (listofIndex.indexOf(index) === -1) {
                    listofIndex.push(index)
                }
            }
            return {
                ...InitiState,
                pageIndex: action.pageIndex,
                pageSize: action.pageSize,
                listofIndex: listofIndex
            }
        default:
            return InitiState;
    }
}


export default newsReducer;