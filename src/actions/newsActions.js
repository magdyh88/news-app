import { ActionType } from './constants';

const newsActions = {
    getAllNews: (news) => {
        return { type: ActionType.GET_ALL_NEWS, news }
    },
    getAllHeadlines: (headlines) => {
        return { type: ActionType.GET_ALL_HEADLINES, headlines }
    },
    getSources: (sources) => {
        return { type: ActionType.GET_ALL_Sources, sources }
    },
    getCountries: (countries) => {
        return { type: ActionType.GET_COUNTRIES, countries }
    },
    updatePageNum:(pageIndex, pageSize, count) => {
        return { type: ActionType.UPDATE_PAGE_NUM, pageIndex, pageSize, count }
    }
}

export default newsActions;