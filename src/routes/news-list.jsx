import * as  React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Actions from '../actions/newsActions';
import API from '../api/apiClient';
import ArticleCard from '../components/article-card';
import Pagination from '../components/pagination';
import FilterSection from '../components/filter-section';
import QuickViewModal from '../components/quick-view-modal';

class NewsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageSize: 5, newsArr: [], sources: [], isOpen: false, selectedArticle: {}
        };

        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    componentWillMount() {
        if (window.location.pathname === '/headlines') {
            API.getHeadlines();
            API.getCountries();
        }
        else {
            API.getAllNews();
        }
        API.getAllSources();
    }

    handleOpenModal(article) {
        this.setState({
            isOpen: true,
            selectedArticle: article
        });
    }

    handleCloseModal() {
        this.setState({
            isOpen: false
        });
    }

    handlePageSizeChange(e) {
        const { Actions, pageIndex, newsArr } = this.props;
        this.setState({
            pageSize: e.target.value
        });
        Actions.updatePageNum(pageIndex, e.target.value, newsArr.length);
    }

    render() {
        const { pageSize, isOpen, selectedArticle } = this.state;
        const { sources, newsArr, countries, pageIndex, listofIndex, isFiltered, API, Actions } = this.props;

        let startIndex = (pageIndex - 1) * parseInt(pageSize);
        let endIndex = startIndex + parseInt(pageSize);

        const dataPage = newsArr ? newsArr.filter((a, index) => [index] >= startIndex && [index] < endIndex) : []

        return <div className="container">
            <div className="row">
                <h3 className="float-left">Home</h3>
            </div>

            <div className="row">
                <FilterSection sources={sources} countries={countries} Api={API} isFiltered={isFiltered} />
            </div>

            <div className="row">
                <label className="ml-auto mr-3">Page Size: </label>
                <select name={'pageSize'} value={pageSize} onChange={this.handlePageSizeChange} className="mr-3" >
                    <option value={5}>5</option>
                    <option value={10}>10</option>
                    <option value={15}>15</option>
                    <option value={20}>20</option>
                </select>
            </div>
            <div className="row">
                {
                    dataPage.length > 0 &&
                    dataPage.map(article => {
                        return <ArticleCard key={article.publishedAt} {...article} openModal={this.handleOpenModal} />
                    })
                }

            </div>

            {
                dataPage.length === 0 && <p className={'text-center'}>No Results Found </p>
            }
            <Pagination listofIndex={listofIndex} pageIndex={pageIndex} actions={Actions} count={newsArr ? newsArr.length : 0} pageSize={pageSize} />
            {
                isOpen && <QuickViewModal isOpen={isOpen} handleCloseModal={this.handleCloseModal} {...selectedArticle} />
            }

        </div>
    }
}

function mapStateToProps(state, ownProps) {
    return {
        newsArr: state.newsArr,
        sources: state.sources,
        pageIndex: state.pageIndex,
        listofIndex: state.listofIndex,
        isFiltered: state.isFiltered,
        countries: state.countries
    }
}


function mapDispatcherToProps(dispatch) {
    return {
        API: bindActionCreators(API, dispatch),
        Actions: bindActionCreators(Actions, dispatch)
    }

}

export default connect(mapStateToProps, mapDispatcherToProps)(NewsList);