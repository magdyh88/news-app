import * as  React from 'react';
import {
    BrowserRouter as Router,
    Route, Link
} from 'react-router-dom';
import News from './news-list';
import ArticleDetails from './article-details';

const Root = () => {
    return (
        <Router>
            <div>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="/">News</a>
                        </div>
                        <ul className="navbar-nav navbar-right">
                            <li className="nav-item"><Link className="nav-link" to="/">Home</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/headlines">Headlines</Link></li>
                        </ul>
                    </div>
                </nav>

                <Route exact path="/" component={News} />
                <Route exact path="/headlines" component={News} />
                <Route path='/artical-details/:title' component={ArticleDetails} />
            </div>
        </Router>
    )
}

export default Root;