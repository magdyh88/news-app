import * as  React from 'react';

const ArticleDetails = (props) => {
    const dateStr = new Date(props.location.articale.publishedAt);

    return (
        <div>
            <div className="card border-light text-left mb-6 p-3 m-3">
                <div className="row no-gutters">
                    <div className="col-md-4">
                        <img src={props.location.articale.urlToImage} className="card-img" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">{props.location.articale.title}</h5>
                            <p><small className="text-muted">Source: {props.location.articale.source.name}</small></p>
                            <p><small className="text-muted">Author: {props.location.articale.author}</small></p>
                            <p><small className="text-muted">Published: {dateStr.toDateString()}</small></p>
                            <a target='_blank' href={props.location.articale.url}>Open Original Post</a>
                        </div>
                    </div>
                </div>
                <div className="row no-gutters mt-3">
                    <p className="card-text">{props.location.articale.content}</p>
                </div>
            </div>
        </div>
    )
}

export default ArticleDetails;