
import NewsAction from '../actions/newsActions';
import { ActionType } from '../actions/constants';

const changePagination = store => next => action => {
    let count = 0, index = 0, size = 0;

    let result = next(action);

    count = store.getState().newsArr ? store.getState().newsArr.length : 0;
    index = store.getState().pageIndex;
    size = store.getState().pageSize;

    if (action.type !== ActionType.UPDATE_PAGE_NUM) {
        store.dispatch(NewsAction.updatePageNum(index, size, count));
    }
    return result;
}

export default changePagination;