import {createStore, applyMiddleware} from "redux";
import newsReducer from './reducers/newsReducer';
import changePagination from './middlewares/paginationMiddleware';
import thunk from 'redux-thunk';

const store = createStore(newsReducer,applyMiddleware(changePagination,thunk));
export default store;